#include <AccelStepper.h>

#define DEG_PER_STEP 1.8
#define STEP_PER_REVOLUTION (360 / DEG_PER_STEP)

#define UNDERLINE_BRAIL 15
#define UNDERLINE_TEXT 17
#define NEXT_BRAIL 50
#define NEXT_CHAR 105

const AccelStepper headStepper(AccelStepper::FULL4WIRE, 5, 4, 3, 2);
const AccelStepper pipeStepper(AccelStepper::FULL4WIRE, 11, 10, 9, 8);

/*
 * -----------------------------------------------------------------------------
 * Code moteurs pas à pas (initialisation, configuration, lancement)
 * -----------------------------------------------------------------------------
 */
char headDir = 1; // 1 = sens horaire, -1 = sens antihoraire
 
void initMotors(){
  headStepper.setAcceleration(200);
  headStepper.setSpeed(200);
  headStepper.setCurrentPosition(0);
  Serial.println("Head stepper motor initialized");
  
  pipeStepper.setAcceleration(150);
  pipeStepper.setSpeed(20);
  pipeStepper.setCurrentPosition(0);
  Serial.println("Pipe stepper motor initialized");
}

void runMotors(){
  headStepper.run();
  pipeStepper.run();
}

void resetMotors(){
  headStepper.setCurrentPosition(0);
  pipeStepper.setCurrentPosition(0);
  headDir = 1;
}

/* 
* -----------------------------------------------------------------------------
* Commande de la tête de l'imprimante
* -----------------------------------------------------------------------------
*/

/*
 * steps : nombre de pas qu'on demande au moteur de parcourir
 */
void moveHeadMotor(int steps){
  headStepper.move(steps*headDir);
  while(headStepper.distanceToGo() != 0){
    headStepper.run();
  }
  
}

/*
 * dir : true = sens horaire, false = sens antihoraire
 */
void setHeadDirection(bool dir){
  headDir = (dir)?1:-1;
}

void inverseHeadDirection(){
  headDir *= -1;
}
 
void pushHead(){
  digitalWrite(1, LOW);
}

void pullHead(){
  digitalWrite(1, HIGH);
}

void nextChar(){
  moveHeadMotor(NEXT_CHAR);
}

void nextBrailHole(){
  moveHeadMotor(NEXT_BRAIL);
}

/* 
 * -----------------------------------------------------------------------------
 * Commande du rouleau de l'imprimante
 * -----------------------------------------------------------------------------
 */

/*
 * steps : nombre de pas qu'on demande au moteur de parcourir
 */
void movePipeMotor(int steps){
  pipeStepper.move((-1)*steps);
  while(pipeStepper.distanceToGo() != 0){
    pipeStepper.run();
  }
}
 
void nextLine(){
  movePipeMotor(UNDERLINE_TEXT);
}

void nextBrailLine(){
  movePipeMotor(UNDERLINE_BRAIL);
}
