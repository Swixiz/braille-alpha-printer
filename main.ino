#include <SoftwareSerial.h>

/* Initialisation des broches */
//extern const int LEDR_PIN = 0; // On définit la led sur la borche 2
const int EMF_PIN = 13;
const int rxPin = 6;
const int txPin = 7;

SoftwareSerial btSerial(rxPin,txPin);

void setup(){
  Serial.begin(9600);
  btSerial.begin(9600);
  log("**************************");
  log("Lancement du programme...");
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  pinMode(EMF_PIN, OUTPUT);
  digitalWrite(EMF_PIN, HIGH);
  initMotors();
  log("Programme lancé !");
}

void loop(){
  int readBuff = btSerial.available();
  
  while(readBuff > 0){
    String readChar = btSerial.readString(); // On lit le caractère
    int remaining = readChar.length()%28;
    int lines = (remaining != 0)?readChar.length()/28+1:readChar.length()/28;
    for(int index = 0; index < lines; index++){
      String l = getLine(readChar, index);
      String trans = translate(l);
      String printing = printFormat(trans);
      Serial.println(index%2);

      int brailLines = 0;
      Serial.println("Received text as lines : " + trans);
      log("Printing line format : " + printing);

        for(int charPos = 0; charPos < printing.length(); charPos++){
          char c = printing.charAt((brailLines%2==1)?printing.length()-1-charPos:charPos);
          Serial.println(c);
          if(c == ':'){
            nextBrailLine();
            brailLines++;
            inverseHeadDirection();
            delay(100);
          }else{
            if(c == '1'){
              digitalWrite(EMF_PIN, LOW);
              delay(100);
              digitalWrite(EMF_PIN, HIGH);
              delay(100);
            }
          if((charPos%2 == 0 && brailLines%2 == 0) || (charPos%2 == 1 && brailLines%2 == 1)){
            nextBrailHole();
          }else{
            int breakPosition = 2*l.length()*(1+brailLines) + brailLines - 1;
            if(charPos != breakPosition){ 
              nextChar();
            }
          }
          delay(200);
          }
        }
      inverseHeadDirection();
      nextLine();
    }
    resetMotors();
    readBuff = btSerial.available(); // On actualise le buffer
  }
  
}

/* Methode executé quand une impression est lancée */
/*void launch(){
  digitalWrite(LEDR_PIN, HIGH); // On éteint la led
  delay(1000);
  digitalWrite(LEDR_PIN, LOW); // On allume la led
  delay(1000);
}*/

void log(String msg){
  Serial.println(msg);
}
