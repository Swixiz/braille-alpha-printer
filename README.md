# Braille Alpha - Printer

Projet E3E ESIEE Paris 2022.
Imprimante braille à base d'Arduino. Inclus une communication Bluetooth avec une application mobile

Cette version est celle présentée pour le Jour des Projets 2022 à ESIEE Paris et a remporté le prix du meilleur projet technique.
Elle ne prend en compte que l'impression d'un certain nombre de caractère et le système de remise à zéro à chaque fin d'impression n'est pas viable.
