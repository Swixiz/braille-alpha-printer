

/*
 * Dictionnaire de braille permettant de traduire chaque caractère ASCI en braille sous forme binaire.
 */
const String brailDic[][26] = {
  {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"},
  
  {"100000","101000","110000","110100","100100","111000","111100","101100","011000","011100","100010","101010",
  "110010","110110","100110","111010","111110","101110","011010","011110","100011","101011", "011101","110011","110111",
  "100111"} // z  
};

String translateChar(String asci){
  for(int index = 0; index < sizeof(brailDic[0])/sizeof(brailDic[0][0]); index++){
    String testChar = brailDic[0][index];
    if(testChar == asci){
      return brailDic[1][index];
    }
  }
  return "";
}


String translate(String sentence){
  String result = "";
  sentence.toUpperCase();
  for(int index = 0; index < sentence.length(); index++){
    String charT = (String)sentence.charAt(index);
    result += translateChar(charT);
  }
  return result;
}

String getLine(String content, int line){
  int contentSize = content.length();
  int lines = contentSize/28;
  int remaining = contentSize % 28;
  if(remaining != 0){
    lines++;
  }
  if(line > lines) return "ERROR";
  return content.substring(line*28, (line==lines)?remaining:(line+1)*28);
}

String printFormat(String line){
  String result = "";
  for(int index = 0; index < line.length()/6; index++){
    result+=line.substring(index*6,index*6+2); 
  }
  result+=":";
  for(int index = 0; index < line.length()/6; index++){
    result+=line.substring(index*6+2,index*6+4); 
  }
  result+=":";
  for(int index = 0; index < line.length()/6; index++){
    result+=line.substring(index*6+4,index*6+6); 
  }
  return result;
}
